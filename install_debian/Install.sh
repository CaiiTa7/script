#!/bin/sh

######
#description: 
#ce script permet de mettre à jour la liste des paquets en fonction de la verision de Debian et d'installer les paquets nécessaires ainsi que pyhton3.12 et VScode
######

# Récupérer la version de Debian installée
VERSION=$(lsb_release -a | grep Codename | awk '{print $2}')

# Ajouter les dépôts nécessaires en fonction de la version de Debian & VScode & Docker
if [ $VERSION = "bullseye" ]
then

    sudo truncate -s 0 /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian bullseye main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian bullseye main contrib non-free" >> /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian-security/ bullseye-security main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian-security/ bullseye-security main contrib non-free" >> /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian bullseye-updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" >> /etc/apt/sources.list
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian bullseye stable" >> /etc/apt/sources.list

elif [ $VERSION = "bookworm" ]
then
    
    sudo truncate -s 0 /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian bookworm main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian bookworm main contrib non-free" >> /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian-security/ bookworm-security main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian-security/ bookworm-security main contrib non-free" >> /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian bookworm-updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian bookworm-updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" >> /etc/apt/sources.list

elif [ $VERSION = "buster" ]
then
    
    sudo truncate -s 0 /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian buster main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian buster main contrib non-free" >> /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian-security/ buster/updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian-security/ buster/updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb http://deb.debian.org/debian buster-updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb-src http://deb.debian.org/debian buster-updates main contrib non-free" >> /etc/apt/sources.list
    echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" >> /etc/apt/sources.list

else
    echo "Version de Debian non supportée"
fi

# Mettre à jour la liste des paquets
sudo apt dist-upgrade -y
sudo apt update -y
sudo apt upgrade -y

# Installer les paquets nécessaires & python3.12
sudo apt install -y build-essential gpg apt-transport-https ca-certificates gnupg lsb-release git curl wget sudo pip python3.12 python3.12-dev python3.12-venv python3.12-distutils python3.12-distutils-extra python3.12-doc python3.12-minimal python3.12-examples python3.12-gdbm

# Installer VScode & Docker
sudo wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo rm -f packages.microsoft.gpg
sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
sudo apt update -y
sudo apt upgrade -y
sudo apt install -y code docker-ce docker-ce-cli docker-compose docker.io

# Installer les guests additions pour VirtualBox
sudo mkdir -p /mnt/cdrom
sudo mount /dev/cdrom /mnt/cdrom
sudo sh ./mnt/cdrom/VBoxLinuxAdditions.run

